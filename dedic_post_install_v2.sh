#!/bin/bash
## Script to automate dedicated server post-install and prepare for transfer##
## written by Artur S.
## v2.0

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m" # No Color
hostname=0
redirect=0
answer=0
email=0
management=0
domain=0
automate=0
ip_main=0
ns_a=0
answer2=0
dnstype=0
ftptype=0
imaptype=0
feature_s=0
def_php_version=0
php_answer=0
php_answer2=0
number_accs=0
version_to_use=0
upgrade_no=0
answer3=0
csf_answer=0
centos_version=0
passwd_mysql=0

echo -e "${GREEN} Now we'll skeep Initial Setup Wizard and setup everything automatically ${NC}"
while true; do
        read -rp "$(echo -e ${GREEN} Proceed with automated Install "(otherwise you'll have to do it manually)" [Type: Y/N] ${NC})" automate
        case "$automate" in
        [nN]* )
        echo "====================================================================================="
        echo -e "${GREEN} Automation module skipped, please proceed with Post Install Manually ${NC}"
        echo "=====================================================================================";
        break;;
        [yY]* )
        echo "================================================="
        echo -e "${GREEN} Proceeding with Automated Module ${NC}"
        echo "================================================="
	touch /etc/.whostmgrft #skips install wizard

### Step 1 Contact Info ###
while true; do
	read -rp "$(echo -e ${GREEN} Enter client email ${NC})" email
	if [[ "$email" =~ ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$ ]]
	then
	echo -e "${GREEN} Email address $email is valid ${NC}"
        break
	else
	echo -e "${RED} Email address $email is invalid ${NC}"
	fi
done

while true; do
	read -rp "$(echo -e ${GREEN} Will you add management for the server? [Y/n] ${NC})" management
	case "$management" in
	[nN]* )
	echo -e "${GREEN} No management, adding only the email you specified ${NC}"
	sed -i "s/^CONTACTEMAIL .*$/CONTACTEMAIL $email/" /etc/wwwacct.conf
	break;;
	[yY]* )
		echo -e " ${GREEN} Which type of management? [Type the number you're prompted] ${NC}"
		select management in "basic" "full"; do
        	case "$management" in
		basic ) echo -e "${GREEN} Adding client's email + hosting-notifications@namecheaphosting.com ${NC}"
		sed -i "s/^CONTACTEMAIL .*$/CONTACTEMAIL $email hosting-notifications@namecheaphosting.com/" /etc/wwwacct.conf
		break;;
		esac
		case "$management" in
		full ) echo -e "${GREEN} Adding only hosting-notifications@namecheaphosting.com ${NC}"
		sed -i "s/^CONTACTEMAIL .*$/CONTACTEMAIL hosting-notifications@namecheaphosting.com/" /etc/wwwacct.conf
		break;;
		esac
		done
	break;;
	* ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
sleep 1;

### Step 2 Networking ###
read -rp "$(echo -e ${GREEN} Enter server hostname ${NC})" hostname
/usr/local/cpanel/bin/set_hostname "$hostname"
sleep 1;

whmapi1 reset_service_ssl_certificate service=exim
/scripts/restartsrv_exim
whmapi1 reset_service_ssl_certificate service=ftp
/scripts/restartsrv_pureftpd
whmapi1 reset_service_ssl_certificate service=dovecot
/scripts/restartsrv_dovecot
whmapi1 reset_service_ssl_certificate service=cpanel
/scripts/restartsrv_cpsrvd
sleep 1;
/usr/local/cpanel/cpkeyclt
sleep 1;

while true; do
        read -rp "$(echo -e ${GREEN} Disable Hostname SSL redirect? [Y/N] ${NC})" redirect
        case "$redirect" in
        [yY]* )
        echo -e "${GREEN} Disabled Hostname SSL redirect ${NC}"
        whmapi1 set_tweaksetting key=alwaysredirecttossl value=0
        whmapi1 set_tweaksetting key=cpredirectssl value="Origin Domain Name"
        whmapi1 set_tweaksetting key=cpredirect value="Origin Domain Name"
        whmapi1 set_tweaksetting key=requiressl value=1;
        break;;
        [nN]* )
        echo -e "${GREEN} Hostname SSL Redirect left intact ${NC}"
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
sleep 1;
sed -i "s/^ETHDEV .*$/ETHDEV eth1/" /etc/wwwacct.conf
sed -i "s/nameserver 4.2.2.3/nameserver 8.8.4.4/g" /etc/resolv.conf
sleep 1;

### Step 3 Setup IP and DNS server ###
ip_main=$(hostname -i) # shows the content of /etc/hosts of ip next to hostname entry, works perfect for CentOS
grep -Fqx "ADDR $ip_main" /etc/wwwacct.conf
greprc="$?"
echo -e "$greprc"
if [ "$greprc" -eq 1 ]
        then
        sed -i "1 i\\ADDR $ip_main" /etc/wwwacct.conf
fi

#DNS
echo -e " ${GREEN} Which Nameserver Software to install? [Type the number you're prompted] ${NC}"
select dnstype in "bind" "nsd" "mydns" "powerdns" "disabled"; do
        case "$dnstype" in
        bind )  echo -e "${GREEN} Installing BIND ${NC}"
        /usr/local/cpanel/scripts/setupnameserver bind --force;
        break;;
        esac
        case "$dnstype" in
        nsd ) echo -e "${GREEN} Installing NSD ${NC}"
        /usr/local/cpanel/scripts/setupnameserver nsd;
        break;;
        esac
        case "$dnstype" in
        mydns ) echo -e "${GREEN} Installing MYDNS ${NC}"
        /usr/local/cpanel/scripts/setupnameserver mydns;
        break;;
        esac
        case "$dnstype" in
        powerdns ) echo -e "${GREEN} Installing POWERDNS ${NC}"
        /usr/local/cpanel/scripts/setupnameserver powerdns;
        break;;
        esac
        case "$dnstype" in
        disabled ) echo -e "${GREEN} Nameserver will be disabled, not installing ${NC}"
        /usr/local/cpanel/scripts/setupnameserver disabled;
        break;;
        esac
done
sleep 1;
/usr/local/cpanel/cpkeyclt #control license reset
sleep 1;

### Step 4 Nameservers + A record ###
read -rp "$(echo -e ${GREEN} Enter client domain ${NC})" domain
sed -i "s/^NS .*$/NS ns1.$domain/" /etc/wwwacct.conf
sed -i "s/^NS2 .*$/NS2 ns2.$domain/" /etc/wwwacct.conf

whmapi1 adddns domain="$hostname" ip="$ip_main" # we add A for HOSTNAME, not for naked domain
while true; do
        read -rp "$(echo -e ${GREEN} Create A records for private nameservers? "(Might cause bugs with named once the domain arrives on the server - ns1 and ns2 need to be in parent zone)" [Y/n] ${NC})" ns_a
        case "$ns_a" in
        [yY]* )
        echo -e "${GREEN} OK, creating A records for private nameservers ${NC}"
        whmapi1 adddns domain=ns1."$domain" ip="$ip_main"
        whmapi1 adddns domain=ns2."$domain" ip="$ip_main";
        break;;
        [nN]* )
        echo -e "${GREEN} No A records will be created for  private nameservers ${NC}";
        break;;
        * ) echo -e "${RED} Please answer yes or no";;
        esac
done
sleep 1;
/usr/local/cpanel/cpkeyclt #control license reset
sleep 1;

### Step 5 Services ###

#FTP
echo -e "${GREEN} Which FTP Software to install? [Type the number you're prompted] ${NC}"
select ftptype in "pure" "pro" "disabled"; do
        case "$ftptype" in
        pure ) echo -e "${GREEN} Installing pure-ftpd ${NC}"
        /scripts/setupftpserver pure-ftpd;
        break;;
        esac
        case "$ftptype" in
        pro ) echo -e "${GREEN} Installing proftpd ${NC}"
        /scripts/setupftpserver proftpd;
        break;;
        esac
        case "$ftptype" in
        disabled ) echo -e "${GREEN} FTP-server will be disabled, not installing ${NC}"
        /scripts/setupftpserver disabled;
        break;;
        esac
done

#IMAP
echo -e "${GREEN} Which IMAP Software to install? [Type the number you're prompted] ${NC}"
select imaptype in "dovecot" "disabled"; do
        case "$imaptype" in
        dovecot ) echo -e "${GREEN} Installing dovecot ${NC}"
        /scripts/setupmailserver dovecot;
        break;;
        esac
        case "$imaptype" in
        disabled ) echo -e "${GREEN} IMAP server will be disabled, not installing ${NC}"
        /scripts/setupmailserver disabled;
        break;;
        esac
done
sleep 1;

### Step 6 Quotas + Feature Showcase ###
echo -e "${GREEN} Setting up quotas ${NC}"
/scripts/initquotas
sleep 1;
while true; do
read -rp "$(echo -e ${GREEN} Disable Feature Showcase from pop-up during WHM login? [Y/N] ${NC})" feature_s
        case "$feature_s" in
        [yY]* )
        touch /var/cpanel/activate/features/disable_feature_showcase # disable annoying Feature Showcase - will no longer pop-up when you login (permanent)
        echo -e "${GREEN} Feature Showcase disabled ${NC}";
        break;;
        [nN]* ) echo -e "${GREEN} Feature Showcase left intact ${NC}";
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
sleep 1;

########### Main steps end here ###########

/usr/local/cpanel/cpkeyclt # control license reset, just in case
sleep 1;

### Installing additional software ###
yum install -y epel-release
yum install -y ea-apache24-mod_proxy_fcgi ea-php54-php-fpm ea-php55-php-fpm ea-php56-php-fpm ea-php70-php-fpm nano atop iotop tcpdump lsof ncdu mc
centos_version=$(awk -F = -F '"' '/VERSION_ID/ {print $2}' /etc/*release)
	if [ "$centos_version" = 7 ]
	then
	echo -e "${GREEN} Installing Centos7-specific packages ${NC}"
	yum remove -y ea-apache24-mod_ruid2
	yum install -y ea-apache24-mod_suphp.x86_64 openssl
	fi
sleep 1;

### PHP options start here ###
echo -e "${GREEN} Changing phploader to IonCube ${NC}"
whmapi1 set_tweaksetting key=phploader value=ioncube
sleep 1;

echo -e "${GREEN} We'll dryrun PHP Handlers to suphp as test ${NC}"
echo
/usr/local/cpanel/bin/rebuild_phpconf --dryrun --ea-php56=suphp --ea-php55=suphp --ea-php54=suphp --ea-php70=suphp #dryrun handlers to see if any changes occurs
echo
echo
while true; do
read -rp "$(echo -e ${GREEN} Proceed with PHP Handlers change? [Y/N] ${NC})" answer
        case "$answer" in
        [yY]* )
        /usr/local/cpanel/bin/rebuild_phpconf --ea-php56=suphp --ea-php55=suphp --ea-php54=suphp --ea-php70=suphp
        echo "=============================="
        echo -e "${GREEN} PHP Handlers Changed ${NC}"
        echo "=============================="
        break;;
        [nN]* ) echo -e "${GREEN} Handlers not changed ${NC}";
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
sleep 1;
/usr/local/cpanel/cpkeyclt #control license reset
sleep 1;

### Getting php default version and setting necessary directives ###
def_php_version=$(whmapi1 php_get_system_default_version |awk ' /data: / {flag=1;next} /command:/{flag=0} flag { print $2}')
echo -e "${GREEN} Systems default version is $def_php_version ${NC}"
echo -e "${GREEN} Systems default php.ini is located in /opt/cpanel/$def_php_version/root/etc/php.ini ${NC}"
sed -i "s/^post_max_size = .*$/post_max_size = 64M/" /opt/cpanel/"$def_php_version"/root/etc/php.ini
echo -e "${GREEN} Set post_max_size to 64M for $def_php_version ${NC}"
sed -i "s/^memory_limit = .*$/memory_limit = 384M/" /opt/cpanel/"$def_php_version"/root/etc/php.ini
echo -e "${GREEN} Set memory_limit to 384M for $def_php_version ${NC}"
sed -i "s/^upload_max_filesize = .*$/upload_max_filesize = 64M/" /opt/cpanel/"$def_php_version"/root/etc/php.ini
echo -e "${GREEN} Set upload_max_filesize to 64M for $def_php_version ${NC}"
sed -i "s/^max_input_vars = .*$/max_input_vars = 4000/" /opt/cpanel/"$def_php_version"/root/etc/php.ini
echo -e "${GREEN} Set max_input_vars to 4000 for $def_php_version ${NC}"
sed -i "s/^max_execution_time = .*$/max_execution_time = 60/" /opt/cpanel/"$def_php_version"/root/etc/php.ini
echo -e "${GREEN} Set max_execution_time to 60 for $def_php_version ${NC}"
sed -i "s/^allow_url_fopen = .*$/allow_url_fopen = On/" /opt/cpanel/"$def_php_version"/root/etc/php.ini
echo -e "${GREEN} Set allow_url_fopen to On for $def_php_version ${NC}"

### Here Feature Showcase options go ###
echo -e "${GREEN} Disabling IMAP Full-Text Search Indexing ${NC}"
/usr/local/cpanel/scripts/uninstall_dovecot_fts

echo -e "${GREEN} Enabling Global DCV Passthrough ${NC}"
whmapi1 set_tweaksetting key=global_dcv_rewrite_exclude value=1

echo -e "${GREEN} Enabling SPF and DKIM for all existing domains ${NC}"
/usr/local/cpanel/scripts/enable_spf_dkim_globally -x

echo -e "${RED} Not installing ModSecurity - skipped ${NC}"

echo -e "${GREEN} Disabling Security Advisor Notifications ${NC}"
whmapi1 set_application_contact_importance app=Check importance=Disabled

echo -e "${GREEN} Restrict document roots to public_html set to OFF ${NC}"
sed -i "s/^publichtmlsubsonly=.*$/publichtmlsubsonly=0/" /var/cpanel/cpanel.config && /usr/local/cpanel/whostmgr/bin/whostmgr2 --updatetweaksettings

### Enable PHP-FPM ###
while true; do
read -rp "$(echo -e ${GREEN} Enable PHP-FPM for all domains by default? [Y/n] ${NC})" php_answer
        case "$php_answer" in
        [nN]* )
        echo -e "${GREEN} PHP-FPM will NOT be enabled by default ${NC}";
        break;;
        [yY]* )
        echo -e "${GREEN} Checking compatibility issues ${NC}"
        whmapi1 get_fpm_count_and_utilization |awk ' /data: / {flag=1;next} /command:/{flag=0} flag { print }'
        while true; do
        read -rp "$(echo -e ${GREEN} Proceed with enabling PHP-FPM for domains by default? [Y/n] ${NC})" php_answer2
                case "$php_answer2" in
                [nN]* )
                echo -e "${GREEN} PHP-FPM will NOT be enabled by default ${NC}";
                break;;
                [yY]* )
                echo -e "${GREEN} Enabling PHP-FPM for domains by default ${NC}"
                whmapi1 php_set_default_accounts_to_fpm default_accounts_to_fpm=1;
                break;;
                * ) echo -e "${RED} Please answer yes or no ${NC}";;
                esac
        done
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
sleep 1;

### Installing CSF ###
csf_install() {
	cd /usr/src
	rm -fv csf.tgz
	wget https://download.configserver.com/csf.tgz
	tar -xzf csf.tgz
	cd csf
	sh install.sh
}
	while true; do
	read -rp "$(echo -e ${GREEN} Install CSF? [Y/n] ${NC})" csf_answer
                case "$csf_answer" in
                [nN]* )
                echo -e "${GREEN} CSF will NOT be installed ${NC}";
                break;;
                [yY]* )
                echo -e "${GREEN} Installing CSF now ${NC}"
		csf_install
		sed -i 's/^TESTING = .*$/TESTING = "0"/' /etc/csf/csf.conf #disable testing mode
		sed -i 's/^IPV6 = .*$/IPV6 = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_EMAIL_ALERT = .*$/LF_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_SU_EMAIL_ALERT = .*$/LF_SU_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_WEBMIN_EMAIL_ALERT = .*$/LF_WEBMIN_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_CONSOLE_EMAIL_ALERT = .*$/LF_CONSOLE_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^PT_USERMEM = .*$/PT_USERMEM = "0"/' /etc/csf/csf.conf
                sed -i 's/^PT_USERRSS = .*$/PT_USERRSS = "0"/' /etc/csf/csf.conf
                sed -i 's/^PT_USERTIME = .*$/PT_USERTIME = "0"/' /etc/csf/csf.conf
                sed -i 's/^PT_USERKILL_ALERT = .*$/PT_USERKILL_ALERT = "0"/' /etc/csf/csf.conf
		csf -r;
		break;;
                * ) echo -e "${RED} Please answer yes or no ${NC}";;
                esac
	done
sleep 1;
/usr/local/cpanel/cpkeyclt #control license reset
sleep 1;

########### MySQL upgrade goes here ###########
echo
echo "=============================="
echo "Preparing for MySQL upgrade"
echo "=============================="
echo
version=$(whmapi1 current_mysql_version |awk 'NR==4 {print $2}')
number_accs=$(ls /var/cpanel/users/* -l |awk -F/ '{print $5}' |grep -cv system)
echo -e "${RED} Current MySQL version is: $version ${NC}"
echo -e "${RED} Number of accounts present in the system: $number_accs ${NC}"
echo
while true; do
read -rp "$(echo -e ${GREEN} Do we need to upgrade MySQL? [Y/N] ${NC})" answer2
        case "$answer2" in
        [nN]* )
        echo -e "${GREEN} Leaving MySQL as is ${NC}";
        exit
        break;;
        [yY]* )
        echo
		if [ "$centos_version" = 7 ]
		then
		echo
		echo "==========================================================================================================================================="
		echo -e "${RED} Your server OS is CENTOS 7, which has no MySQL password in the /root/.my.cnf file. Reset the root MySQL password to proceed ${NC}"
		echo "==========================================================================================================================================="
		echo
		echo -e "${RED} Resetting local MySQL root password now ${NC}"
		passwd_mysql=$(openssl rand -base64 14)
		echo -e "${GREEN} Your new local root MySQL password is $passwd_mysql ${NC}"
		echo -e "${GREEN} Don't worry, it will be added to /root/.my.cnf file ${NC}"
		whmapi1 set_local_mysql_root_password password="$passwd_mysql"
		sleep 1;
		echo
		echo -e "${GREEN} MySQL Password Reset Completed ${NC}"
		sleep 1;
		else
		echo -e "${GREEN} Server OS is CENTOS 6, we can proceed safely ${NC}"
		fi
	echo -e "${GREEN} List of available versions ${NC}"
        whmapi1 installable_mysql_versions |awk ' /versions: / {flag=1;next} /metadata:/{flag=0} flag { print }'
        echo
                echo -e "${GREEN} Which version to use? [Type the number next to the version] ${NC}"
                select version_to_use in "5.0" "5.1" "5.5" "5.6" "10.0" "10.1" "10.2"; do   ## using select + writing all cases instead of loop is an optimal choice, otherwise there's no reliable input control - it's whmapi's flaw ##
                        case "$version_to_use" in
                        5.0 ) echo -e "${GREEN} We will upgrade to version 5.0 ${NC}"
                        whmapi1 start_background_mysql_upgrade version=5.0 > /root/upgrade_process1;
                        break;;
                        esac
                        case "$version_to_use" in
                        5.1 ) echo -e "${GREEN} We will upgrade to version 5.1 ${NC}"
                        whmapi1 start_background_mysql_upgrade version=5.1 > /root/upgrade_process1;
                        break;;
                        esac
                        case "$version_to_use" in
                        5.5 ) echo -e "${GREEN} We will upgrade to version 5.5 ${NC}"
                        whmapi1 start_background_mysql_upgrade version=5.5 > /root/upgrade_process1;
                        break;;
                        esac
                        case "$version_to_use" in
                        5.6 ) echo -e "${GREEN} We will upgrade to version 5.6 ${NC}"
                        whmapi1 start_background_mysql_upgrade version=5.6 > /root/upgrade_process1;
                        break;;
                        esac
                        case "$version_to_use" in
                        10.0 ) echo -e "${GREEN} We will upgrade to version 10.0 ${NC}"
                        whmapi1 start_background_mysql_upgrade version=10.0 > /root/upgrade_process1;
                        break;;
                        esac
                        case "$version_to_use" in
                        10.1 ) echo -e "${GREEN} We will upgrade to version 10.1 ${NC}"
                        whmapi1 start_background_mysql_upgrade version=10.1 > /root/upgrade_process1;
                        break;;
                        esac
                        case "$version_to_use" in
                        10.2 ) echo -e "${GREEN} We will upgrade to version 10.2 ${NC}"
                        whmapi1 start_background_mysql_upgrade version=10.2 > /root/upgrade_process1;
                        break;;
                        esac
                done
        echo
        upgrade_no=$(awk '/upgrade_id: / {print $2}' /root/upgrade_process1)
        echo -e "${GREEN} The upgrade is in backgroung. You can check logs in /var/cpanel/logs/$upgrade_no/unattended_background_upgrade.log ${NC}"
        echo
        echo -e "${GREEN} Checking Upgrade Status ${NC}"
        whmapi1 background_mysql_upgrade_status upgrade_id="$upgrade_no"
        sleep 1;
        rm -f /root/upgrade_process1
        echo
        while true; do
        read -rp "$(echo -e ${GREEN} Watch MySQL upgrade log? [Y/N] ${NC})" answer3
                case "$answer3" in
                [yY]* )
                echo -e "${GREEN} Attaching to the upgrade log. Once you see 'MariaDB upgrade completed successfully' line - you can close the log and exit ${NC}"
                tailf /var/cpanel/logs/"$upgrade_no"/unattended_background_upgrade.log;
                break;;
                [nN]* )
                echo -e "${RED} The upgrade will continue in the background. Don't forget to check logs in /var/cpanel/logs/$upgrade_no/unattended_background_upgrade.log ${NC}";
                break;;
                * ) echo -e "${RED} Please answer yes or no ${NC}";;
                esac
                done
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
