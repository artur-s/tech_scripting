#!/bin/bash
## Script to automate dedicated server post-install and prepare for transfer##
## written by Artur S.
## v4.6

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m" # No Color

echo -e "${GREEN} Now we'll skeep Initial Setup Wizard and setup everything automatically ${NC}"
while true; do
        read -rp "$(echo -e ${GREEN} Proceed with automated Post-Install "(otherwise you'll have to do it manually)"? [Type: Y/N] ${NC})" automate
        case "$automate" in
        [nN]* )
        echo "====================================================================================="
        echo -e "${GREEN} Automation module skipped, please proceed with Post Install Manually ${NC}"
        echo "=====================================================================================";
        break;;
        [yY]* )
        echo "================================================="
        echo -e "${GREEN} Proceeding with Automated Module ${NC}"
        echo "================================================="
	touch /etc/.whostmgrft #skips install wizard

### Step 1 Contact Info ###
while true; do
	read -rp "$(echo -e ${GREEN} Enter client email ${NC})" email
	if [[ "$email" =~ ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,9}$ ]]
	then
	echo -e "${GREEN} Email address $email is valid ${NC}"
        break
	else
	echo -e "${RED} Email address $email is invalid ${NC}"
	fi
done

while true; do
	read -rp "$(echo -e ${GREEN} Will you add management for the server? [Y/n] ${NC})" management
	case "$management" in
	[nN]* )
	echo -e "${GREEN} No management, adding only the email you specified ${NC}"
	sed -i "s/^CONTACTEMAIL .*$/CONTACTEMAIL $email/" /etc/wwwacct.conf
	break;;
	[yY]* )
		echo -e " ${GREEN} Which type of management? [Type the number you're prompted] ${NC}"
		select management in "basic" "full"; do
        	case "$management" in
		basic ) echo -e "${GREEN} Adding client's email ${NC}"
		sed -i "s/^CONTACTEMAIL .*$/CONTACTEMAIL $email/" /etc/wwwacct.conf && /scripts/restartsrv_named
		break;;
		esac
		case "$management" in
		full ) echo -e "${GREEN} Adding only hosting-notifications@namecheaphosting.com ${NC}"
		sed -i "s/^CONTACTEMAIL .*$/CONTACTEMAIL hosting-notifications@namecheaphosting.com/" /etc/wwwacct.conf && /scripts/restartsrv_named
		break;;
		esac
		done
	break;;
	* ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done

while true; do
	read -rp "$(echo -e ${GREEN} Enter client domain ${NC})" domain
	if [[ "$domain" =~ ^(([a-zA-Z0-9](-?[a-zA-Z0-9])*)\.)*[a-zA-Z0-9](-?[a-zA-Z0-9])+\.[a-zA-Z]{2,}$ ]]
        then
	echo -e "${GREEN} $domain is valid ${NC}"
        break
	else
	echo -e "${RED} $domain is invalid ${NC}"
        fi
done

#### read -rp "$(echo -e ${GREEN} Use standard hostname for the server "(like server1.domain.tld)"? [Y/n] ${NC})" standard
##
#
#### read -rp "$(echo -e ${GREEN} Install Softaculous? [Y/n] ${NC})" softac

whmapi1 get_fpm_count_and_utilization |awk ' /data: / {flag=1;next} /command:/{flag=0} flag { print }'
### read -rp "$(echo -e ${GREEN} Enable PHP-FPM for all domains by default? [Y/n] ${NC})" php_answer
#
##read -rp "$(echo -e ${GREEN} Install CSF? [Y/n] ${NC})" csf_answer
###
version=$(whmapi1 current_mysql_version |awk 'NR==4 {print $2}')
number_accs=$(ls /var/cpanel/users/* -l |awk -F/ '{print $5}' |grep -cv system)
echo -e "${RED} Current MySQL version is: $version ${NC}"
echo -e "${RED} Number of accounts present in the system: $number_accs ${NC}"
echo
####read -rp "$(echo -e ${GREEN} Do we need to upgrade MySQL? [Y/N] ${NC})" answer2
sleep 1;

### Step 2 Networking ###
while true; do
	read -rp "$(echo -e ${GREEN} Use standard hostname for the server "(like server1.domain.tld)"? [Y/n] ${NC})" standard
        case "$standard" in
        [nN]* )
        echo -e "${GREEN} Non-standard hostname was selected. Please enter the hostname manually ${NC}"
		while true; do
                read -rp "$(echo -e ${GREEN} Enter server hostname ${NC})" hostname
                       	if [[ "$hostname" =~ ^(([a-zA-Z](-?[a-zA-Z0-9])*)\.)*[a-zA-Z](-?[a-zA-Z0-9])+\.[a-zA-Z]{2,}$ ]]
                        then
                       	echo -e "${GREEN} $hostname is valid hostname ${NC}"
                        /usr/local/cpanel/bin/set_hostname "$hostname"
			break
                        else
                        echo -e "${RED} $hostname is invalid hostname ${NC}"
                        fi
		done
        break;;
        [yY]* )
                /usr/local/cpanel/bin/set_hostname "server1.$domain"
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done

whmapi1 reset_service_ssl_certificate service=exim
/scripts/restartsrv_exim
whmapi1 reset_service_ssl_certificate service=ftp
/scripts/restartsrv_pureftpd
whmapi1 reset_service_ssl_certificate service=dovecot
/scripts/restartsrv_dovecot
whmapi1 reset_service_ssl_certificate service=cpanel
/scripts/restartsrv_cpsrvd
sleep 1;
/usr/local/cpanel/cpkeyclt
sleep 1;
        echo -e "${GREEN} Disabled Hostname SSL redirect ${NC}"
        whmapi1 set_tweaksetting key=alwaysredirecttossl value=0
        whmapi1 set_tweaksetting key=cpredirectssl value="Origin Domain Name"
        whmapi1 set_tweaksetting key=cpredirect value="Origin Domain Name"
        whmapi1 set_tweaksetting key=requiressl value=1;
sleep 1;
grep -q "Virtual" /proc/cpuinfo
if [ "$?" -eq 0 ]
        then
	sed -i "s/^ETHDEV .*$/ETHDEV eth0/" /etc/wwwacct.conf
	else
	sed -i "s/^ETHDEV .*$/ETHDEV eth1/" /etc/wwwacct.conf
fi

sed -i "s/nameserver 4.2.2.3/nameserver 8.8.4.4/g" /etc/resolv.conf
sleep 1;

### Step 3 Setup IP and DNS server ###
ip_main=$(hostname -i) # shows the content of /etc/hosts of ip next to hostname entry, works perfect for CentOS
grep -Fqx "ADDR $ip_main" /etc/wwwacct.conf
greprc="$?"
echo -e "$greprc"
if [ "$greprc" -eq 1 ]
        then
        sed -i "1 i\\ADDR $ip_main" /etc/wwwacct.conf
fi

#DNS
        echo -e "${GREEN} Installing BIND ${NC}"
        /usr/local/cpanel/scripts/setupnameserver bind --force;
sleep 1;
/usr/local/cpanel/cpkeyclt #control license reset
sleep 1;

### Step 4 Nameservers + A record ###
sed -i "s/^NS .*$/NS ns1.$domain/" /etc/wwwacct.conf
sed -i "s/^NS2 .*$/NS2 ns2.$domain/" /etc/wwwacct.conf

whmapi1 adddns domain="$hostname" ip="$ip_main" # we add A for HOSTNAME, not for naked domain
echo -e "${GREEN} Creating A records for private nameservers ${NC}"
IP1=`ip a | grep global | sed 's|.* \([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\)/[0-9]\+.*|\1|g' |  head -1`
IP2=`ip a | grep global | sed 's|.* \([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\)/[0-9]\+.*|\1|g' | tail -n+2 | head -1`
    if [ -z $IP2 ];then          		#we have only one IP
        IP2=$IP1
    fi
whmapi1 adddns domain=ns1."$domain" ip="$IP1"
whmapi1 adddns domain=ns2."$domain" ip="$IP2";
sleep 1;
/usr/local/cpanel/cpkeyclt #control license reset
sleep 1;

### Step 5 Services ###

#FTP
        echo -e "${GREEN} Installing pure-ftpd ${NC}"
        /scripts/setupftpserver pure-ftpd;

#IMAP
        echo -e "${GREEN} Installing dovecot ${NC}"
        /scripts/setupmailserver dovecot;
sleep 1;

### Step 6 Quotas + Feature Showcase ###
echo -e "${GREEN} Setting up quotas ${NC}"
/scripts/initquotas
sleep 1;
        touch /var/cpanel/activate/features/disable_feature_showcase # disable annoying Feature Showcase - will no longer pop-up when you login (permanent)
        echo -e "${GREEN} Feature Showcase disabled ${NC}";
sleep 1;

########### Main steps end here ###########

/usr/local/cpanel/cpkeyclt # control license reset, just in case
sleep 1;

### Installing additional software ###
yum install -y epel-release
yum install -y ea-apache24-mod_proxy_fcgi ea-php54-php-fpm ea-php55-php-fpm ea-php56-php-fpm ea-php70-php-fpm ea-php71-php-fpm ea-php72-php-fpm ea-php73-php-fpm ea-apache24-mod_suphp.x86_64 openssl nano atop iotop tcpdump lsof ncdu mc
yum install -y ea-php56-php-mbstring ea-php70-php-mbstrinCg ea-php71-php-mbstring ea-php72-php-mbstring ea-php73-php-fpm ea-php56-php-intl ea-php70-php-intl ea-php71-php-intl ea-php72-php-intl ea-php73-php-intl
sleep 1;

### PHP options start here ###
echo -e "${GREEN} Changing phploader to IonCube ${NC}"
whmapi1 set_tweaksetting key=phploader value=ioncube
sleep 1;

echo -e "${GREEN} We'll dryrun PHP Handlers to suphp as test ${NC}"
echo
/usr/local/cpanel/bin/rebuild_phpconf --dryrun --ea-php56=suphp --ea-php70=suphp --ea-php71=suphp --ea-php72=suphp #dryrun handlers to see if any changes occurs
echo
echo
        /usr/local/cpanel/bin/rebuild_phpconf --ea-php54=suphp --ea-php55=suphp --ea-php56=suphp --ea-php70=suphp --ea-php71=suphp --ea-php72=suphp
        echo "=============================="
        echo -e "${GREEN} PHP Handlers Changed ${NC}"
        echo "=============================="
sleep 1;
/usr/local/cpanel/cpkeyclt #control license reset
sleep 1;

### Getting php default version and setting necessary directives ###
def_php_version=$(whmapi1 php_get_system_default_version |awk ' /data: / {flag=1;next} /command:/{flag=0} flag { print $2}')
echo -e "${GREEN} Systems default version is $def_php_version ${NC}"
echo -e "${GREEN} Systems default php.ini is located in /opt/cpanel/$def_php_version/root/etc/php.ini ${NC}"
sed -i "s/^post_max_size = .*$/post_max_size = 64M/" /opt/cpanel/*/root/etc/php.ini
echo -e "${GREEN} Set post_max_size to 64M for all php versions ${NC}"
sed -i "s/^memory_limit = .*$/memory_limit = 384M/" /opt/cpanel/*/root/etc/php.ini
echo -e "${GREEN} Set memory_limit to 384M for all php versions ${NC}"
sed -i "s/^upload_max_filesize = .*$/upload_max_filesize = 64M/" /opt/cpanel/*/root/etc/php.ini
echo -e "${GREEN} Set upload_max_filesize to 64M for all php versions ${NC}"
sed -i "s/^max_input_vars = .*$/max_input_vars = 4000/" /opt/cpanel/*/root/etc/php.ini
echo -e "${GREEN} Set max_input_vars to 4000 for all php versions ${NC}"
sed -i "s/^max_execution_time = .*$/max_execution_time = 60/" /opt/cpanel/*/root/etc/php.ini
echo -e "${GREEN} Set max_execution_time to 60 for all php versions ${NC}"
sed -i "s/^allow_url_fopen = .*$/allow_url_fopen = On/" /opt/cpanel/*/root/etc/php.ini
echo -e "${GREEN} Set allow_url_fopen to On for all php versions ${NC}"

### Here Feature Showcase options go ###
echo -e "${GREEN} Disabling IMAP Full-Text Search Indexing ${NC}"
/usr/local/cpanel/scripts/uninstall_dovecot_fts

echo -e "${GREEN} Enabling Global DCV Passthrough ${NC}"
whmapi1 set_tweaksetting key=global_dcv_rewrite_exclude value=1

echo -e "${GREEN} Enabling SPF and DKIM for all existing domains ${NC}"
/usr/local/cpanel/scripts/enable_spf_dkim_globally -x

echo -e "${RED} Not installing ModSecurity - skipped ${NC}"

echo -e "${GREEN} Disabling Security Advisor Notifications ${NC}"
whmapi1 set_application_contact_importance app=Check importance=Disabled

echo -e "${GREEN} Enabling Awstats in cPanel ${NC}"
whmapi1 set_tweaksetting key=skipawstats value=0

echo -e "${GREEN} Restrict document roots to public_html set to OFF ${NC}"
whmapi1 set_tweaksetting key=publichtmlsubsonly value=0

echo -e "${GREEN} Adding JSON Perl Module ${NC}"
/usr/local/cpanel/bin/cpanm --notest --notest --verbose --perl=/scripts/cpan_sandbox/x86_64/perl JSON::XS

echo -e "${GREEN} Adding Switch Perl module ${NC}"
/usr/local/cpanel/bin/cpanm --notest --notest --verbose --perl=/scripts/cpan_sandbox/x86_64/perl Switch

echo -e "${GREEN} Adding HTTPS Perl module ${NC}"
/usr/local/cpanel/bin/cpanm --notest --notest --verbose --perl=/scripts/cpan_sandbox/x86_64/perl LWP::Protocol::https

###  Safe Password Policy by default for cPanel users ###
echo -e "${GREEN} Adding Safe Password Policy by default for cPanel users ${NC}"
whmapi1 setminimumpasswordstrengths passwd=60

### Increasing MySQL max_allowed_packet by default ###
echo -e "${GREEN} Increased MySQL max_allowed_packet to maximum ${NC}"
sed -i '/\[mysqld\]/a max_allowed_packet=1073741824' /etc/my.cnf
sed -i "s/^max_allowed_packet=.*$/max_allowed_packet=1073741824/" /etc/my.cnf

### Disabling MySQL Srtict Mode to the way it works on shared servers ###
echo -e "${GREEN} Disabled MySQL STRICT MODE ${NC}"
sed -i '/\[mysqld\]/a sql_mode=NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' /etc/my.cnf

## Control MySQL restart
/scripts/restartsrv_mysql
sleep 1;

### Agree to EULA tgerms ###
whmapi1 accept_eula

while true; do
	read -rp "$(echo -e ${GREEN} Install Softaculous? [Y/n] ${NC})" softac
        case "$softac" in
        [nN]* )
        echo -e "${GREEN} Softaculous will not be installed ${NC}"
        break;;
        [yY]* )
        echo -e " ${GREEN} Installing Softaculous ${NC}"
	whmapi1 set_tweaksetting key=phploader value=ioncube && wget -N http://files.softaculous.com/install.sh && chmod 0755 install.sh && ./install.sh
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
sleep 1;

### Enable PHP-FPM ###
while true; do
	read -rp "$(echo -e ${GREEN} Enable PHP-FPM for all domains by default? [Y/n] ${NC})" php_answer
        case "$php_answer" in
        [nN]* )
        echo -e "${GREEN} PHP-FPM will NOT be enabled by default ${NC}";
       	break;;
        [yY]* )
        echo -e "${GREEN} Enabling PHP-FPM for domains by default ${NC}"
        whmapi1 php_set_default_accounts_to_fpm default_accounts_to_fpm=1;
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
sleep 1;

### Installing CSF ###
csf_install() {
	cd /usr/src
	rm -fv csf.tgz
	wget https://download.configserver.com/csf.tgz
	tar -xzf csf.tgz
	cd csf
	sh install.sh
}
	while true; do
		read -rp "$(echo -e ${GREEN} Install CSF? [Y/n] ${NC})" csf_answer
                case "$csf_answer" in
                [nN]* )
                echo -e "${GREEN} CSF will NOT be installed ${NC}";
                break;;
                [yY]* )
                echo -e "${GREEN} Installing CSF now ${NC}"
		csf_install
		sed -i 's/^TESTING = .*$/TESTING = "0"/' /etc/csf/csf.conf #disable testing mode
		sed -i 's/^IPV6 = .*$/IPV6 = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_EMAIL_ALERT = .*$/LF_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_SU_EMAIL_ALERT = .*$/LF_SU_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_WEBMIN_EMAIL_ALERT = .*$/LF_WEBMIN_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_CONSOLE_EMAIL_ALERT = .*$/LF_CONSOLE_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
                sed -i 's/^PT_USERMEM = .*$/PT_USERMEM = "0"/' /etc/csf/csf.conf
                sed -i 's/^PT_USERRSS = .*$/PT_USERRSS = "0"/' /etc/csf/csf.conf
                sed -i 's/^PT_USERTIME = .*$/PT_USERTIME = "0"/' /etc/csf/csf.conf
                sed -i 's/^PT_USERKILL_ALERT = .*$/PT_USERKILL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_PERMBLOCK_ALERT = .*$/LF_PERMBLOCK_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LF_NETBLOCK_ALERT = .*$/LF_NETBLOCK_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^LT_EMAIL_ALERT = .*$/LT_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^RT_RELAY_ALERT = .*$/RT_RELAY_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^RT_AUTHRELAY_ALERT = .*$/RT_AUTHRELAY_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^RT_POPRELAY_ALERT = .*$/RT_POPRELAY_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^RT_LOCALHOSTRELAY_ALERT = .*$/RT_LOCALHOSTRELAY_ALERT = "0"/' /etc/csf/csf.conf
		sed -i 's/^CT_EMAIL_ALERT = .*$/CT_EMAIL_ALERT = "0"/' /etc/csf/csf.conf
		csf -r && service lfd restart;
		break;;
                * ) echo -e "${RED} Please answer yes or no ${NC}";;
                esac
	done
sleep 1;
/usr/local/cpanel/cpkeyclt #control license reset
sleep 1;


########### MySQL upgrade goes here ###########
echo
echo "============="
echo "MySQL status"
echo "============="
echo
echo -e "${RED} Current MySQL version is: $version ${NC}"
echo -e "${RED} Number of accounts present in the system: $number_accs ${NC}"
echo
while true; do
	read -rp "$(echo -e ${GREEN} Do we need to upgrade MySQL? [Y/N] ${NC})" answer2
        case "$answer2" in
        [nN]* )
        echo -e "${GREEN} Leaving MySQL as is ${NC}";
        exit
        break;;
        [yY]* )
        echo
	centos_version=$(awk -F = -F '"' '/VERSION_ID/ {print $2}' /etc/*release)
		if [ "$centos_version" = 7 ]
		then
		echo
		echo "==========================================================================================================================================="
		echo -e "${RED} Your server OS is CENTOS 7, which has no MySQL password in the /root/.my.cnf file. Reset the root MySQL password to proceed ${NC}"
		echo "==========================================================================================================================================="
		echo
		echo -e "${RED} Resetting local MySQL root password now ${NC}"
		passwd_mysql=$(openssl rand -base64 14)
		echo -e "${GREEN} Your new local root MySQL password is $passwd_mysql ${NC}"
		echo -e "${GREEN} Don't worry, it will be added to /root/.my.cnf file"
		whmapi1 set_local_mysql_root_password password="$passwd_mysql"
		sleep 1;
		echo
		echo -e "${GREEN} MySQL Password Reset Completed ${NC}"
		sleep 1;
		else
		echo -e "${GREEN} Server OS is CENTOS 6, we can proceed safely ${NC}"
		fi
	echo -e "${GREEN} List of available versions ${NC}"
        whmapi1 installable_mysql_versions |awk ' /versions: / {flag=1;next} /metadata:/{flag=0} flag { print }'
        echo
                echo -e "${GREEN} Which version to use? [Type the number next to the version] ${NC}"
		while true; do
		read -rp "$(echo -e ${GREEN} Type the version to use:  ${NC})" version_to_use
			if [[ "$version_to_use" =~ ^[0-9]+\.[0-9]+ ]]
	        	then
			echo -e "${GREEN} $version_to_use is valid ${NC}"
			echo -e "${GREEN} We will upgrade to version $version_to_use ${NC}"
			whmapi1 start_background_mysql_upgrade version="$version_to_use" > /root/upgrade_process1;
	        	break
			else
			echo -e "${RED} $version_to_use is invalid ${NC}"
			fi
		done
        echo
        upgrade_no=$(awk '/upgrade_id: / {print $2}' /root/upgrade_process1)
        echo -e "${GREEN} The upgrade is in backgroung. You can check logs in /var/cpanel/logs/$upgrade_no/unattended_background_upgrade.log ${NC}"
        echo
        echo -e "${GREEN} Checking Upgrade Status ${NC}"
        whmapi1 background_mysql_upgrade_status upgrade_id="$upgrade_no"
        sleep 1;
        rm -f /root/upgrade_process1
        echo
        while true; do
        read -rp "$(echo -e ${GREEN} Watch MySQL upgrade log? [Y/N] ${NC})" answer3
                case "$answer3" in
                [yY]* )
                echo -e "${GREEN} Attaching to the upgrade log. Once you see 'MariaDB upgrade completed successfully' line - you can close the log and exit ${NC}"
                tailf /var/cpanel/logs/"$upgrade_no"/unattended_background_upgrade.log;
                break;;
                [nN]* )
                echo -e "${RED} The upgrade will continue in the background. Don't forget to check logs in /var/cpanel/logs/$upgrade_no/unattended_background_upgrade.log ${NC}";
                break;;
                * ) echo -e "${RED} Please answer yes or no ${NC}";;
                esac
                done
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
        break;;
        * ) echo -e "${RED} Please answer yes or no ${NC}";;
        esac
done
